/**
 * Created by rhizalpatra on 3/8/2017.
 */
(function () {
    var Message;
    Message = function (arg) {
        this.name = arg.name, this.text = arg.text, this.message_side = arg.message_side;
        this.draw = function (_this) {
            return function () {
                var $message;
                $message = $($('.message_template').clone().html());

                $message.addClass(_this.message_side).find('.chat-img').addClass('pull-' + _this.message_side);
                $message.addClass(_this.message_side).find('#avatar').attr('src','/uploads/avatars/' + arg.avatar);
                $message.addClass(_this.message_side).find('.name').addClass('pull-' + _this.message_side);
                $message.addClass(_this.message_side).find('.text').addClass('pull-' + _this.message_side);

                $message.addClass(_this.message_side).find('.name').html(_this.name);
                $message.addClass(_this.message_side).find('.text').html(_this.text);
                $('.messages').prepend($message);
                return setTimeout(function () {
                    return $message.addClass('appeared');
                }, 0);
            };
        }(this);
        return this;
    };
    $(function () {
        var drawMessage, message_side, sendMessage, getTwitts;
        message_side = 'right';

        getTwitts = function () {
            $.ajax({
                url: '/get_twitts',
                type: 'GET',
                success: function (data) {
                    // console.log(data);
                    if (data.twitts != null) {
                        data.twitts.sort();
                        data.twitts.forEach(function (twitt) {
                            drawMessage(twitt);
                        });
                    }
                    //
                }
            });
        };
        drawMessage = function (twitt) {
            var $messages, message;
            if (twitt.message.trim() === '') {
                return;
            }
            $('.message_input').val('');
            $messages = $('.messages');
            message_side = twitt.is_me === true ? 'right' : 'left';
            message = new Message({
                name: twitt.name,
                text: twitt.message,
                avatar:twitt.avatar,
                message_side: message_side
            });
            message.draw();
            return $messages.animate({scrollTop: $messages.prop('scrollHeight')}, 300);
        };
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': CSRF_TOKEN,
            }
        });
        sendMessage = function () {
            var message = $(".message_input").val();
            $.ajax({
                url: '/post_twitt',
                type: 'POST',
                dataType: 'json',
                data: {message:message},
                dataType: 'JSON',
                success: function (data) {
                    if (data.twitt != null) {
                        drawMessage(data.twitt);
                    }
                }
            });

        };
        $('.send_message').click(function (e) {
            return sendMessage();
        });
        $('.message_input').keyup(function (e) {
            if (e.which === 13) {
                return sendMessage();
            }
        });

        $(document).ready(function () {
            getTwitts();
        });

    });


}.call(this));

