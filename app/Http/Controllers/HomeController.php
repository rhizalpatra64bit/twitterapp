<?php

namespace App\Http\Controllers;

use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function twitt()
    {
        if (Auth::guest())return redirect()->route('login');
        return view('twitt');
    }

    public function profile()
    {
        return view('profile', array('user' => Auth::User()));
    }

    /**
     * Store or update a newly created resource in storage.
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $id = 0)
    {
        $twitt = Twitt::updateOrCreate(['id' => $id], $request->input());
        $twitt->is_me = true;
        $twitt->name = Auth::User()->name;
        return response()->json(compact('twitt'));
    }

    public function update_avatar(Request $request)
    {
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(150, 150)->save(public_path('uploads/avatars/' . $filename));
            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }
        return redirect()->action('HomeController@profile');
    }

    public function update_profile(Request $request)
    {

        $user = Auth::user(); // User::find($request->input('id'));

        if ($user){
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));

            $user->save();

            Session::flash('message', 'Profile anda telah di update.');
            Session::flash('message_type', 'success');

            return redirect()->action('HomeController@profile');
        }

    }
}
