<?php
namespace App\Http\Controllers;

use App\Twitt;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TwittController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $twitts = DB::table('twitts')
            ->join('users', 'users.id', '=', 'twitts.user_id')
            ->select('users.name','users.avatar', 'twitts.*')
//            ->orderBy('created_at', 'desc')
            ->get();

        foreach ($twitts as $t) {
            if ($t->user_id == Auth::User()->id) {
                $t->is_me = true;
            }
        }

        return response()->json(compact('twitts'));
    }

    /**
     * Store or update a newly created resource in storage.
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->input();

        $data['user_id'] = Auth::User()->id;
        $data['created_at'] = date('Y-m-d H:i:s');

        $twitt = Twitt::Create($data);
        $twitt->is_me = true;
        $twitt->name = Auth::User()->name;
        $twitt->avatar = Auth::User()->avatar;
        return response()->json(compact('twitt'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $twitt = Twitt::find($id);

        return response()->json(compact('twitt'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $success = Twitt::destroy($id);

        return response()->json(compact('success'));
    }
}