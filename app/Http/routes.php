<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::auth();

Route::get('/', 'HomeController@twitt');

Route::get('/home', 'HomeController@index');

Route::get('/profile', 'HomeController@profile');

Route::post('/update_profile', 'HomeController@update_profile');

Route::post('/update_avatar', 'HomeController@update_avatar');

Route::get('/get_twitts', 'TwittController@index');

Route::post('/post_twitt', 'TwittController@store');
