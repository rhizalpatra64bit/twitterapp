
@extends('layouts.app')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="container">
        <div class="row">
            <div class="col-md-12">

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="input-group">
                            <input id="btn-input" type="text" class="form-control input-sm message_input"
                                   placeholder="Update Status..."/>
                            <span class="input-group-btn">
                            <button class="btn btn-primary btn-sm send_message" id="btn-chat">Update</button>
                        </span>
                        </div>
                    </div>
                    <div class="panel-body">
                        <ul class="chat messages">
                        </ul>
                    </div>
                    <div class="panel-footer">
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="message_template">
        <li class="clearfix">
            <span class="chat-img">
                <img id="avatar" style="width: 65px;" src="" alt="User Avatar" class="img-circle"/>
            </span>
            <div class="chat-body clearfix">
                <div class="header">
                    <strong class="primary-font name"></strong>
                </div>
                <hr>
                <p class="text"></p>
            </div>
        </li>
    </div>

@endsection

@push('styles')
<link href="{{ asset('css/chat.css')  }}" media="all" rel="stylesheet" type="text/css" />
@endpush

@push('scripts')
<script src="{{asset('js/chat.js')}}"></script>
@endpush