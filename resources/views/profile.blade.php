<?php
/**
 * Created by PhpStorm.
 * User: rhizalpatra
 * Date: 3/8/2017
 * Time: 14:39
 */
?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if (Session::has('message'))
                <div class="alert alert-{{ Session::get('message_type') }}">{{ Session::get('message') }}</div>
            @endif
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">User Area</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <!-- SIDEBAR USERPIC -->

                                <div class="col-md-12 text-center">
                                    <label>Click Image</label>
                                </div>

                                <div class="profile-userpic">
                                    <img id="avatar" src="/uploads/avatars/{{ $user->avatar }}" class="img-responsive" alt="">
                                </div>

                                <form  class="form-horizontal" enctype="multipart/form-data" action="/update_avatar" method="POST">
                                    {{ csrf_field() }}
                                    <input type="file" style="visibility: hidden" name="avatar">

                                    <div class="col-md-12 text-center">
                                        <input type="submit" value="Upload Photo" class="btn btn-sm btn-primary">
                                    </div>
                                </form>

                            </div>
                            <div class="col-md-7 well">
                                <form enctype="multipart/form-data" action="/update_profile" class="form-horizontal"
                                      method="POST">
                                    {{ csrf_field() }}
                                    <fieldset>

                                        <!-- Form Name -->
                                        <legend>Update Profile</legend>

                                        <!-- Text input-->
                                        <div class="form-group">

                                            <div class="col-md-12">
                                                <input id="name" name="name" placeholder="Full Name"
                                                       class="form-control input-md" required="required"
                                                       value="{{ $user->name }}" type="text">

                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input id="email" name="email" placeholder="Email"
                                                       class="form-control input-md" required="required"
                                                       value="{{ $user->email }}" type="email">

                                            </div>
                                        </div>

                                        <!-- Password input-->
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input id="password" name="password" placeholder="Password"
                                                       class="form-control input-md" required="" type="password">

                                            </div>
                                        </div>

                                    </fieldset>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="submit" class="btn btn-primary pull-right" value="Save">

                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@push('scripts')
<script type="text/javascript">
    $('#avatar').on('click',function () {
       $('input[name="avatar"]').click();
    });
</script>
@endpush

<style>


    .profile-userpic img {
        float: none;
        margin: 0 auto;
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important;
    }
</style>